/*jshint globals: true, browser: true, jquery: true, -W117*/
$(document).ready(function () {
    var showScore = $("#showScore");
    var nickForm = $("#setNick");
    var nickError = $("#nickError");
    var nickBox = $("#nickname");
    var users = $("#users");
    var messageForm = $("#send-message");
    var messageBox = $("#message");
    var chat = $("#chat");
    var socket;

    if (!socket || !socket.connected) {
        socket = io({
            forceNew: true
        });
    }

    socket.on("sendScore", function (horses) {
        $(".sekcja").remove();
        var content = "";
        $("#showScore").append("<table class='sekcja'><tr><td>Numer Startowy</td><td>Średnia</td></tr></table>");
        for (var i = 0; i < horses.length; i++) {
            content +=
                "<tr><td>" + horses[i].numerStartowy + "</td>" +
                "<td>" + horses[i].srednia + "</td></tr>";
        }
        $(".sekcja").append(content);
    });

    socket.on("each", function (result) {
        $("#showEachScore").show();
        $("#pojedynczy").append("<tr><td>" + result.startingNumber + "</td>" + "<td>" + result.competA +
            "</td>" + "<td>" + result.competB + "</td>" + "<td>" + result.competC + "</td>" +
            "<td>" + result.competD + "</td>" + "<td>" + result.competE + "</td>" + "<td>" + result.judgeId + "</td></tr>");
        $("#showEachScore").delay(5000).fadeOut(1000);
        //  alert("Nowy wynik! \n Numer startowy konia: " + result.startingNumber + "\n Ocena kategorii pierwszej: " + result.competA +
        //  "\n Ocena kategorii drugiej: " + result.competB + "\n Ocena kategorii trzeciej: " + result.competC +
        //  "\n Ocena kategorii czwartej: " + result.competD + "\n Ocena kategorii piątej: " + result.competE +
        //  "\n Oceniał sędzia z identyfikatorem: " + result.judgeId);
    });

    socket.on("eachEdited", function (result) {
        $("#showEditedScore").show();
        $("#edytowana").append("<tr><td>" + result.startingNumber + "</td>" + "<td>" + result.competA +
            "</td>" + "<td>" + result.competB + "</td>" + "<td>" + result.competC + "</td>" +
            "<td>" + result.competD + "</td>" + "<td>" + result.competE + "</td>" + "<td>" + result.judgeId + "</td></tr>");
        $("#showEditedScore").delay(5000).fadeOut(1000);
        //  alert("Edytowany wynik! \n Numer startowy konia: " + result.startingNumber + "\n Ocena kategorii pierwszej: " + result.competA +
        //  "\n Ocena kategorii drugiej: " + result.competB + "\n Ocena kategorii trzeciej: " + result.competC +
        //  "\n Ocena kategorii czwartej: " + result.competD + "\n Ocena kategorii piątej: " + result.competE +
        //  "\n Oceniał sędzia z identyfikatorem: " + result.judgeId);
    });

    nickForm.submit(function (event) {
        event.preventDefault();
        socket.emit("new user", nickBox.val(), function (data) {
            if (data) {
                $("#nickWrap").hide();
                $("#contentWrap").show();
            } else {
                nickError.html("To imię jest zajęte, spróbuj innego");
            }
        });
        nickBox.val("");
    });

    socket.on("usernames", function (data) {
        var html = "";
        for (var i = 0; i < data.length; i++) {
            html += data[i] + "<br />";
        }
        users.html(html);
    });

    messageForm.submit(function (event) {
        event.preventDefault();
        socket.emit("send message", messageBox.val(), function (data) {
            chat.append("<span class='error'>" + data + "</span><br />");
        });
        messageBox.val("");
    });

    socket.on("load old msg", function (docs) {
        for (var i = 0; i < docs.length; i++) {
            displayMsg(docs[i]);
        }
    });

    socket.on("new message", function (data) {
        displayMsg(data);
    });

    function displayMsg(data) {
        chat.append("<span class='msg'><b>" + data.nick + ": </b>" + data.msg + "</span><br />");
    }

    socket.on("whisper", function (data) {
        chat.append("<span class='whisper'><b>" + data.nick + ": </b>" + data.msg + "</span><br />");
    });
});