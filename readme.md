## Uruchomienie przykładu

Będąc w katalogu głównym projektu należy wpisać następujące instrukcje:

  - `npm install`
  - `bower install`
  - `node index.js`
  
  Następnie, w przeglądarce otwieramy adres 
[http://localhost:8000/](http://localhost:8000/).