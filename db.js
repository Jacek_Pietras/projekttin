/*jshint node: true*/

var mongoose = require("mongoose");
var async = require("async");


mongoose.connect("mongodb://localhost/competition", function (err) {
    if (err) {
        console.log("Błąd podczas łączenia z bazą danych: " + err);
    } else {
        console.log("Pomyślnie połączono z bazą danych");
    }
});


var Horses = mongoose.model("horses", new mongoose.Schema({
    _id: String,
    startNumber: Number,
    competitionA: Number,
    competitionB: Number,
    competitionC: Number,
    competitionD: Number,
    competitionE: Number
}));


var ChatSchema = mongoose.model("Message", new mongoose.Schema({
    nick: String,
    msg: String,
    created: {
        type: Date,
        default: Date.now
    }
}));


var Judges = mongoose.model("judges", new mongoose.Schema({
    _id: String,
    name: String,
    surname: String,
    key: Number
}));


var judge = new Judges({
    _id: mongoose.Types.ObjectId(),
    name: "Jan",
    surname: "Kowalski",
    key: Math.floor(Math.random() * (9999 - 1000 + 1) + 1000)
});


var judge2 = new Judges({

    _id: mongoose.Types.ObjectId(),

    name: "Marek",

    surname: "Witkowski",

    key: Math.floor(Math.random() * (9999 - 1000 + 1) + 1000)

});


var judge3 = new Judges({
    _id: mongoose.Types.ObjectId(),
    name: "Darek",
    surname: "Borgos",
    key: Math.floor(Math.random() * (9999 - 1000 + 1) + 1000)
});


var judge4 = new Judges({
    _id: mongoose.Types.ObjectId(),
    name: "Bartosz",
    surname: "Markowski",
    key: Math.floor(Math.random() * (9999 - 1000 + 1) + 1000)

});


var judge5 = new Judges({
    _id: mongoose.Types.ObjectId(),
    name: "Adam",
    surname: "Langosz",
    key: Math.floor(Math.random() * (9999 - 1000 + 1) + 1000)
});


var judge6 = new Judges({
    _id: mongoose.Types.ObjectId(),
    name: "Mariusz",
    surname: "Bytkowski",
    key: Math.floor(Math.random() * (9999 - 1000 + 1) + 1000)
});


var judge7 = new Judges({
    _id: mongoose.Types.ObjectId(),
    name: "Zbigniew",
    surname: "Majek",
    key: Math.floor(Math.random() * (9999 - 1000 + 1) + 1000)
});


var judge8 = new Judges({
    _id: mongoose.Types.ObjectId(),
    name: "Grzegorz",
    surname: "Braun",
    key: Math.floor(Math.random() * (9999 - 1000 + 1) + 1000)
});


var judge9 = new Judges({
    _id: mongoose.Types.ObjectId(),
    name: "Andrzej",
    surname: "Majchrowski",
    key: Math.floor(Math.random() * (9999 - 1000 + 1) + 1000)
});


var judge10 = new Judges({
    _id: mongoose.Types.ObjectId(),
    name: "Janusz",
    surname: "Borkowski",
    key: Math.floor(Math.random() * (9999 - 1000 + 1) + 1000)
});


var addJudges = function (judgeValues) {
    return function (callback) {
        Judges.findOne({
            name: judgeValues.name,
            surname: judgeValues.surname
        }, function (error, result) {
            if (error) {
                console.log(error);
            } else {
                if (result === null) {
                    judgeValues.save(function (err) {
                        if (err) {
                            console.log(err);
                        }
                        callback(err);
                    });
                } else {
                    console.log("Podany sędzia znajduje się już w bazie danych");
                }
            }
        });
    };
};

var pushToDatabase = function (callback) {
    async.parallel([addJudges(judge), addJudges(judge2), addJudges(judge3),
                   addJudges(judge4), addJudges(judge5), addJudges(judge6),
                   addJudges(judge7), addJudges(judge8), addJudges(judge9),
                   addJudges(judge10)],
        function (err) {
            if (err) {
                console.log(err);
            } else {
                callback();
                process.exit(0);
            }
        });
};

async.series([pushToDatabase]);